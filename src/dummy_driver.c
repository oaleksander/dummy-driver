#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("oaleksander");
MODULE_DESCRIPTION("A dummy device driver");

// Параметры устройства
#define DEVICE_NAME "my_dummy_device"
const size_t DATA_SIZE = 100;
const int DEVICE_COUNT = 1;
const int MINOR_NUMBER = 0;

struct dummy_device_state // Информация об устрройстве
{
    struct cdev *_cdev; // Идентификатор символьного устройства
    char data[100]; // Данные
    size_t size; 
};
struct dummy_device_state device_data;

dev_t dev_num; // Старший и младший номер устройства
struct class* p_class; // Класс устройства

int return_code; // Переменная для экономии места в стеке

#define print_dmesg(level, msg, ...) \
    printk(level DEVICE_NAME ": " msg "\n", ##__VA_ARGS__);

// Файловые операции

int device_open(struct inode *_inode, struct file *_file)
{
    print_dmesg(KERN_INFO, "Opened device.");
    return 0;
}

int device_close(struct inode *_inode, struct file *_file)
{
    print_dmesg(KERN_INFO, "Device closed.");
    return 0;
}

ssize_t device_read(struct file *_file, char *__user buffer, size_t count, loff_t *offset)
{
    count = min(device_data.size - (size_t)*offset, count);
    if (count <= 0)
        return 0;

    if (copy_to_user(buffer, device_data.data + *offset, count))
        return -EFAULT;


    print_dmesg(KERN_INFO, "Read, copied %zu bytes to user ", count);

    *offset += count;
    return count;
}

ssize_t device_write(struct file *_file, const char *buffer, size_t count, loff_t *offset)
{
    count = min(device_data.size - (size_t)*offset, count);
    if (count <= 0)
        return 0;

    if (copy_from_user(device_data.data + *offset, buffer, count))
        return -EFAULT;

    print_dmesg(KERN_INFO, "Write, copied %zu bytes from user ", count);

    *offset += count;
    return count;
}

struct file_operations fops = {
    .owner = THIS_MODULE,
    .open = device_open,
    .release = device_close,
    .write = device_write,
    .read = device_read};

// Загрузка модуля, создание устройства
static int driver_entry(void)
{
    print_dmesg(KERN_INFO, "Loading module.");

    // Выделение идентификатора
    return_code = alloc_chrdev_region(&dev_num, MINOR_NUMBER, DEVICE_COUNT, DEVICE_NAME);
    if (return_code < 0)
    {
        print_dmesg(KERN_ALERT, "Failed to allocate major number.");
        return return_code;
    }
    print_dmesg(KERN_INFO, "Major number is %u.", MAJOR(dev_num));
   
    device_data.size = DATA_SIZE;

    // Выделение устройства
    device_data._cdev = cdev_alloc();
    if(device_data._cdev  == NULL) {
        print_dmesg(KERN_ALERT, "Failed to allocate character device.");
        return -1;
    }
    device_data._cdev->ops = &fops;
    device_data._cdev->owner = THIS_MODULE;

    // Инициализация устройства
    return_code = cdev_add(device_data._cdev, dev_num, DEVICE_COUNT);
    if (return_code < 0)
    {
        print_dmesg(KERN_ALERT, "Unable to add character device to kernel");
        return return_code;
    }

    // Создание класса устройства
    p_class = class_create(THIS_MODULE, DEVICE_NAME);
    if (IS_ERR(p_class))
    {
        print_dmesg(KERN_WARNING, "Can't create device class.");
        unregister_chrdev_region(dev_num, DEVICE_COUNT);
        return -1;
    }

    // Создание символьного устройства /dev/DEVICE_NAME
    if (IS_ERR(device_create(p_class, NULL, dev_num, NULL, DEVICE_NAME)))
    {
        print_dmesg(KERN_WARNING, "Can't create device file.");
        class_destroy(p_class);
        unregister_chrdev_region(dev_num, 1);
        return -1;
    }

    return 0;
}

// Удаление устройства, выгрузка модуля
static void driver_exit(void)
{
    device_destroy(p_class, dev_num);
    class_destroy(p_class); 
    cdev_del(device_data._cdev);
    unregister_chrdev_region(dev_num, DEVICE_COUNT);
    print_dmesg(KERN_INFO, "Unloaded module.");
}

module_init(driver_entry);
module_exit(driver_exit);
