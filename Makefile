MODULE_NAME = dummy_driver

SRC := src/dummy_driver.c

$(MODULE_NAME)-objs = $(SRC:.c=.o)
obj-m := $(MODULE_NAME).o

PWD := $(CURDIR)

all: 
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) -I$(PWD)/src modules
	rm -rf .*.*.cmd *.mod* **/*.o* **/.*.o* *.o *.symvers *.order
	
 
clean: 
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) -I$(PWD)/src clean